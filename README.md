# pex-worksample-vue

Live demo: https://pex-worksample-vue.netlify.app

## Quick Start

NOTE: If you're running in dev mode, performance optimizations will not kick in, and the app might be slower.

```bash
# install dependencies
$ npm install

# Run app in dev mode
$ npm run dev

# generate & serve static project
$ npm run generate
$ npm run start
```

When running locally, the app can be viewed at http://localhost:3000.  Data used to generate the app can be viewed at http://localhost:3000/data.

Data for this was copied over from my [previous worksample](https://gitlab.com/SomeKittens/pex-worksample).

Built with [Nuxt.js](https://nuxtjs.org) and [AntD Vue](https://www.antdv.com).

## Worksample Requirements

https://gist.github.com/ivanpk/8688798a4f3c7f3e44ce715417d1f3d8

> Please build a page that allows for searching and filtering of a movies API that would return responses like the example.

Searchbar is at the top of the page, and searches over a movie's title, actors, and genre.

> Requirements:
> - Use Vue.js, HTML, and a CSS preprocessor like SASS

This submission uses Vue (+ Nuxt) and the SASS preprocessor (technically SCSS, but who's counting).  I also used HTML :)

> - The constructed payload should be visible - either through the console or some other explicit rendering.

Payload can be found at https://pex-worksample-vue.netlify.app/data

> - Submission should be sent in a GitHub repo.

I hope GitLab is ok.
