export interface Movie {
  id: number
  title: string
  genre: {
    id: number
    title: string
  }[]
  actors: {
    id: number
    name: string
  }[]
  is_series: boolean
  release_date: string
  poster: string | null
  description: string
  details?: string[]
  show?: boolean
}
