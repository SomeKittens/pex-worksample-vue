import movieDetails from './movieData.json'
import { Movie } from '~/types'

// Returns an array of notable strings
const pluckMovieDetails = (movie: Movie) => {
  return [
    movie.title,
    ...movie.genre.map((g) => g.title),
    ...movie.actors.map((a) => a.name),
  ].map((str) => str.toLowerCase())
}

export const state = () =>
  movieDetails.map((movie) => ({
    ...movie,
    show: true,
    details: pluckMovieDetails(movie),
  }))

export const mutations = {
  filter(state: Movie[], query: string) {
    query = query.toLowerCase()

    state.forEach((movie) => {
      movie.show = movie.details?.some((detail) => detail.includes(query))
    })
  },
}
